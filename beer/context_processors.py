from blog.models import Tag

def categories(request):
    return {'categories':Tag.objects.filter(featured=True)}