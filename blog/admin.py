from django.contrib import admin
from blog.models import Post, Tag

class PostAdmin(admin.ModelAdmin):
    fields = ('title','slug','body','author','draft','tags_human')
    readonly_fields = ['created','updated']
    prepopulated_fields = {'slug':('title',)}

admin.site.register(Post, PostAdmin)
admin.site.register(Tag)