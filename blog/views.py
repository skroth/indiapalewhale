from django.shortcuts import render
from blog.models import Post

def index(request):
    if request.user.is_staff:
        posts = Post.objects.all()
    else:
        posts = Post.objects.filter(draft=False)
    posts = posts.order_by('-created')

    return render(request, "index.html", {'posts':posts})