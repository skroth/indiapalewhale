from django.db import models
from django.contrib.auth.models import User

class Post(models.Model):
    title = models.CharField(max_length=150)
    slug = models.SlugField(max_length=152, unique=True)
    body = models.TextField()
    body_html = models.TextField()
    author = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    draft = models.BooleanField(default=True)

    tags_human = models.TextField("tags", blank=True)
    tags = models.ManyToManyField('Tag')

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        # Here we do the backend work that represents the user changing the
        # tags for the Post. Handles creating the initial post and also
        # modifying exists post's tags. Only tags_human gets changed by user,
        # then we take care of translating that to the database.
        # Also, we do the conversion from user-inputted body text to the
        # html representation of the body that we use for the template.
        # For now we simply copy it over, but eventually we'll throw in a
        # markdown processor.

        self.body_html = self.body  # TODO: Eventually put markdown stuff here

        # Split the tag string by comma, then remove white space around edges
        # and convert to lowercase. It's weird to have 'foo' and 'FOO' to be
        # different tags.
        tag_strs = map(lambda x: x.strip().lower(), self.tags_human.split(','))
        tag_objects = map(lambda x: Tag.objects.get_or_create(name=x)[0],
                          tag_strs)

        # Put the formatted tag strings back together for the human string
        self.tags_human = ', '.join(tag_strs)

        super(Post, self).save(*args, **kwargs)

        # Remove tags that need to go
        for tag in self.tags.all():
            if tag not in tag_objects:
                self.tags.remove(tag)

        # Do the actual DB relationship between post and tags after we already
        # created the post. Database can't make the relationship until a post
        # has a primary key. (which it receives when saved)
        if tag_objects:
            for tag in tag_objects:
                self.tags.add(tag)


class Tag(models.Model):
    name = models.CharField(max_length=100)
    display_name = models.CharField(max_length=100, blank=True)
    featured = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.display_name:
            self.display_name = self.name
        super(Tag, self).save(*args, **kwargs)
