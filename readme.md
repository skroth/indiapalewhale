After you clone this repo, you may have to rename the directory from indiapalewhale to beer. That's the name of the django project, so it may break if the project directory doesn't match the name of project.

I think you can accomplish the same thing by specifying the directory name when you do the clone:
For ssh: `git clone git@bitbucket.org:skroth/indiapalewhale.git beer`
For https: `git clone https://skroth@bitbucket.org/skroth/indiapalewhale.git beer`
